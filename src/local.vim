"
" `:source shortcuts.vim` and you will have the following mappings:
"
"         Build and run:    <F9>
"         Build only:       <C-F9>
"

map <F9> :call BK_buildrun()<CR>
map <F10> :call BK_build()<CR>
map <F6> :!cd ../build/; cgdb berserkore.dbg<CR>
map <F5> :call BK_run()<CR>

function! BK_ExecInGui(cmd)
  " Run the `cmd` in gnome-terminal instead of the current shell
  "
  " TODO: implement for woe32 as well
  "
  exec "!gnome-terminal -x bash -c '".a:cmd."; read -p \"Hit something to continue...\" -n1'"
endfunction

function! BK_runcmd(cmd)
  " Execute a shell command
  " If vim running in a GUI then execute in an external terminal
  "
  if has("gui_running")
    let result = BK_ExecInGui(a:cmd)
  else
    silent exec ":!" . a:cmd
    let result = v:shell_error
  endif
  return result
endfunction

function! BK_scons()
  " Build the project with the build system
  "
  return BK_runcmd("cd ..; scons")
endfunction

function! BK_run()
  " Run the game
  "
  !cd ..; ./optirun.dbg.sh
endfunction

function! BK_build()
  " Build; wait on laugh at programmer on failure.
  "
  let result = BK_scons()
  if result != 0
    call BK_pause()
  else
    redraw!
    echo "build successful..."
  endif
  return result
endfunction

function! BK_buildrun()
  " Build; run if succeeded
  "
  if BK_build() == 0
    call BK_run()
  endif
endfunction

function! BK_pause()
  silent !echo; read -n1 -p 'You can do it! You are NOT a failure!'
  redraw!
endfunction

au! BufWritePost local.vim :silent so local.vim
